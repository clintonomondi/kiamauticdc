<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Kiamauticdc</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="/assets/images/logo.png" rel="icon">
    <link href="/assets/images/logo.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
    <link href="assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
    <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/slider.css" rel="stylesheet">



    <!-- =======================================================
    * Template Name: Mentor - v4.3.0
    * Template URL: https://bootstrapmade.com/mentor-free-education-bootstrap-theme/
    * Author: BootstrapMade.com
    * License: https://bootstrapmade.com/license/
    ======================================================== -->
</head>

<body>

<!-- ======= Header ======= -->
<header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

        <h1 class="logo me-auto"><a href="index.php">Kiamauticdc</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html" class="logo me-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

        <nav id="navbar" class="navbar order-last order-lg-0">
            <ul>
                <li><a  <?php if($_SERVER['SCRIPT_NAME']=="/index.php") { ?>  class="active"   <?php   } else{ ?>class="" <?php }?> href="index.php">Home</a></li>
                <li><a  <?php if($_SERVER['SCRIPT_NAME']=="/about.php") { ?>  class="active"   <?php   } else{ ?>class="" <?php }?> href="about.php">About</a></li>
                <li><a  <?php if($_SERVER['SCRIPT_NAME']=="/services.php") { ?>  class="active"   <?php   } else{ ?>class="" <?php }?> href="services.php">Services</a></li>
                <li><a  <?php if($_SERVER['SCRIPT_NAME']=="/gallary.php") { ?>  class="active"   <?php   } else{ ?>class="" <?php }?> href="gallary.php">Gallery</a></li>
                <li><a  <?php if($_SERVER['SCRIPT_NAME']=="/contact.php") { ?>  class="active"   <?php   } else{ ?>class="" <?php }?> href="contact.php">Contact</a></li>
            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->

        <a href="#" class="get-started-btn">Donate</a>

    </div>
</header><!-- End Header -->