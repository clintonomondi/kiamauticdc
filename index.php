<?php
require_once 'navbar.php'
?>

<!--   ======= Hero Section =======-->
  <section id="hero" class="d-flex justify-content-center align-items-center">
    <div class="container position-relative" data-aos="zoom-in" data-aos-delay="100">
      <h1>E.A.P.C Kiamauti child development centre (KE0340)</h1>
      <h2>We help in addressing children’s needs with the main goal of releasing children from poverty and all its forms in Jesus name</h2>
      <a href="/about.php" class="btn-get-started">Read More</a>
    </div>
  </section><!-- End Hero -->


  <main id="main">

    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container" data-aos="fade-up">

        <div class="row">
          <div class="col-lg-6 order-1 order-lg-2" data-aos="fade-left" data-aos-delay="100">
            <img src="assets/images/slider2.jpeg" class="img-fluid" alt="">
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 content">
            <h3>Background information </h3>
            <p class="fst-italic">
                E.A.P.C Kiamauti child development centre (KE0340) is a Christian based community organization established in 2006
                at Kiamauti E.A.P.C church, to help in addressing children’s needs with the main goal of releasing children from poverty
                and all its forms in Jesus name. It was established in partnership with compassion Int’ Kenya and
                the larger Kiamauti community. It operates strictly on Christian principle.
            </p>
            <ul>
              <li><i class="bi bi-check-circle"></i> STEWARDSHIP</li>
              <li><i class="bi bi-check-circle"></i> INTEGRITY.</li>
              <li><i class="bi bi-check-circle"></i> MORALITY.</li>
              <li><i class="bi bi-check-circle"></i> EFFICIENCY.</li>
            </ul>
          </div>
        </div>

      </div>
    </section><!-- End About Section -->

    <!-- ======= Counts Section ======= -->
    <section id="counts" class="counts section-bg">
      <div class="container">

        <div class="row counters">

          <div class="col-lg-3 col-6 text-center">
            <span data-purecounter-start="0" data-purecounter-end="430" data-purecounter-duration="1" class="purecounter"></span>
            <p>Total beneficiaries</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-purecounter-start="0" data-purecounter-end="374" data-purecounter-duration="1" class="purecounter"></span>
            <p>Sponsored</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-purecounter-start="0" data-purecounter-end="236" data-purecounter-duration="1" class="purecounter"></span>
            <p>Youths</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-purecounter-start="0" data-purecounter-end="9" data-purecounter-duration="1" class="purecounter"></span>
            <p>Trainers</p>
          </div>

        </div>

      </div>
    </section><!-- End Counts Section -->

    <!-- ======= Why Us Section ======= -->
    <section id="why-us" class="why-us">
      <div class="container" data-aos="fade-up">

        <div class="row justify-content-center">
          <div class="col-lg-4 d-flex align-items-stretch">
            <div class="content">
              <h3>Mission Statement	</h3>
              <p>
                  EAPC KIAMAUTI CDC exists to advocates the needs of children in disadvantaged families by providing programs that give opportunities to children to release
                  them from spiritual, social, economic and physical poverty in Jesus name.
              </p>
              <div class="text-center">
                <a href="/about.php" class="more-btn">Learn More <i class="bx bx-chevron-right"></i></a>
              </div>
            </div>
          </div>
          <div class="col-lg-8 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-boxes d-flex flex-column justify-content-center">
              <div class="row">
                <div class="col-xl-6 d-flex align-items-stretch">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="bx bx-receipt"></i>
                    <h4>Vision Statement</h4>
                    <p>EAPC KIAMAUTI CDC will be an excellent child development center that will see all registered beneficiaries in the society lead a dignified life.	</p>
                  </div>
                </div>
                <div class="col-xl-6 d-flex align-items-stretch ">
                  <div class="icon-box mt-6 mt-xl-0 about">
                    <i class="bx bx-cube-alt"></i>
                    <h4>About us</h4>
                    <p>Since its establishment EAPC Kiamauti CDC (KE0340) has given hope to ..<a href="/about.php">read more..</a></p>
                  </div>
                </div>
              </div>
            </div><!-- End .content-->
          </div>
        </div>

      </div>
    </section><!-- End Why Us Section -->





    <!-- ======= Trainers Section ======= -->
    <section id="trainers" class="trainers">
      <div class="container" data-aos="fade-up">

        <div class="row" data-aos="zoom-in" data-aos-delay="100">
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="member">
              <img src="assets/images/mike.png" class="img-fluid" alt="" style="width: 500px; height: 500px;">
              <div class="member-content">
                <h4>Michael Otieno</h4>
                <span>Project Director </span>
                <p>
                Thank you for being interested to know about Kiamauti CDC , we assure our partners and stakeholders of excellent service delivery to array_fill
that need our services in the society.To archive excellent service delivery to humanity we invite all willing partners collaborators,well wishers and volunteers to join hands with us in any way they can to achieve this goal. Thank you and welcome to Kiamauti CDC                </p>
                <div class="social">
                  <a href=""><i class="bi bi-twitter"></i></a>
                  <a href=""><i class="bi bi-facebook"></i></a>
                  <a href=""><i class="bi bi-instagram"></i></a>
                  <a href=""><i class="bi bi-linkedin"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="member">
              <img src="assets/img/health.jpg " class="img-fluid" alt="" style="width: 500px; height: 500px;">
              <div class="member-content">
                <h4>Judith Gitonga</h4>
                <span>Project Health Worker</span>
                <p>
                  In the physical domain we teach our beneficiaries to choose good health practices and be physically healthy.
                  We also teach our beneficiaries to interact with other people in a healthy and friendly manner. we protect our beneficiaries
                  from all forms of abuse because we believe protection of children is central to Gods heart for them .Our approach to child protection begins with preventing abuse as possible.
                  We commit to protecting the children in our centre from all sort of abuse and exploitation ,to intervene quickly and to seek restoration and healing for children affected by abuse.
</p>
                <div class="social">
                  <a href=""><i class="bi bi-twitter"></i></a>
                  <a href=""><i class="bi bi-facebook"></i></a>
                  <a href=""><i class="bi bi-instagram"></i></a>
                  <a href=""><i class="bi bi-linkedin"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="member">
              <img src="assets/img/makunyi.jpg" class="img-fluid" alt="" style="width: 500px; height: 500px;">
              <div class="member-content">
                <h4>Zakayas  Makunyi</h4>
                <span>Project Accountant</span>
                <p>
                  Hebrew 4:13 Nothing in all creation is hidden from God's sight.Everything is uncovered and laid bare before the eyes of Him to whom
                  we must give account
                  Commitment to simplicity in accountability, easy to be understood by the led nd leading teams is our passion.We serve knowing we shall finally give an account to God
                </p>
                <div class="social">
                  <a href=""><i class="bi bi-twitter"></i></a>
                  <a href=""><i class="bi bi-facebook"></i></a>
                  <a href=""><i class="bi bi-instagram"></i></a>
                  <a href=""><i class="bi bi-linkedin"></i></a>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Trainers Section -->

  </main><!-- End #main -->

 <?php
require_once  'footer.php'

?>