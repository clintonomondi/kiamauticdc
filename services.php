<?php
require_once 'navbar.php'
?>

<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <div class="breadcrumbs" data-aos="fade-in">
        <div class="container">
            <h2>Our Services</h2>
            <p>EAPC KIAMAUTI CDC exists to advocates the needs of children in disadvantaged families by providing programs that give opportunities to children to release them from spiritual, social, economic and physical poverty in Jesus name. </p>
        </div>
    </div><!-- End Breadcrumbs -->

    <!-- ======= Events Section ======= -->
    <section id="events" class="events">
        <div class="container" data-aos="fade-up">

            <div class="row">
                <div class="col-md-6 d-flex align-items-stretch">
                    <div class="card">
                        <div class="card-img">
                            <img src="assets/images/services/s1.jpeg" alt="...">
                        </div>
                        <div class="card-body">
                            <h5 class="card-title"><a href="#">
                                    Education support to need children and youths</a></h5>
                            <p class="fst-italic text-center"></p>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur elit, sed do eiusmod tempor ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 d-flex align-items-stretch">
                    <div class="card">
                        <div class="card-img">
                            <img src="assets/images/services/s2.jpeg" alt="...">
                        </div>
                        <div class="card-body">
                            <h5 class="card-title"><a href="#">
                                    Empowering youth self help groups</a></h5>
                            <p class="fst-italic text-center"></p>
                            <p class="card-text">Sed ut perspiciatis unde omnis iste natus error sit voluptatem doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo</p>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-6 d-flex align-items-stretch">
                    <div class="card">
                        <div class="card-img">
                            <img src="assets/images/services/s3.jpeg" alt="...">
                        </div>
                        <div class="card-body">
                            <h5 class="card-title"><a href="#">

                                    Empowering caregiver self help groups</a></h5>
                            <p class="fst-italic text-center"></p>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur elit, sed do eiusmod tempor ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 d-flex align-items-stretch">
                    <div class="card">
                        <div class="card-img">
                            <img src="assets/images/services/s4.jpeg" alt="...">
                        </div>
                        <div class="card-body">
                            <h5 class="card-title"><a href="#">
                                    Evangelism</a></h5>
                            <p class="fst-italic text-center"></p>
                            <p class="card-text">Sed ut perspiciatis unde omnis iste natus error sit voluptatem doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo</p>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </section><!-- End Events Section -->

</main><!-- End #main -->




<?php
require_once 'footer.php'
?>
