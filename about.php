

<?php
require_once 'navbar.php'
?>
<main id="main">
    <!-- ======= Breadcrumbs ======= -->
    <div class="breadcrumbs" data-aos="fade-in">
        <div class="container">
            <h2>About Us</h2>
            <p>EAPC KIAMAUTI CDC will be an excellent child development center that will see all registered beneficiaries in the society lead a dignified life. </p>
        </div>
    </div><!-- End Breadcrumbs -->

    <!-- ======= About Section ======= -->
    <section id="about" class="about">
        <div class="container" data-aos="fade-up">

            <div class="row">
                <div class="col-lg-6 order-1 order-lg-2" data-aos="fade-left" data-aos-delay="100">
                    <img src="assets/images/slider2.jpeg" class="img-fluid" alt="">
                </div>
                <div class="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 content">
                    <h3>Background information.</h3>
                    <p class="fst-italic">
                        E.A.P.C Kiamauti child development centre (KE0340) is a Christian based community organization established in 2006
                        at Kiamauti E.A.P.C church, to help in addressing children’s needs with the main goal of releasing children from poverty and
                        all its forms in Jesus name. It was established in partnership
                        with compassion Int’ Kenya and the larger Kiamauti community. It operates strictly on Christian principle.
                    </p>
                    <p>
                        Since its establishment EAPC Kiamauti CDC (KE0340) has given hope to children, their families and community
                        in efforts to fighting poverty and all its forms. Currently it has a center based sponsorship capacity of 500 children and
                        youth, out of which the center currently has a beneficiary count of 417,
                        that is 374 sponsored and 29 unsponsored and 14 survival beneficiaries.
                    </p>
                    <p>The center in collaboration with different development partners and financial aid from compassion Int' Kenya has being providing 
                        Education sponsorship through paying school fees, buying school uniforms and
                         ,books for the center beneficiary children and youth among others and as well carrying out child protection
                          measures to ensure children are safe in their environment , the caregivers have also been undergoing various
                          s empowerment programs to alleviate poverty in the all family and ultimately the whole community. </p>

                </div>
            </div>

        </div>
    </section><!-- End About Section -->

    <!-- ======= Counts Section ======= -->
    <section id="counts" class="counts section-bg">
        <div class="container">

            <div class="row counters">

                <div class="col-lg-3 col-6 text-center">
                    <span data-purecounter-start="0" data-purecounter-end="430" data-purecounter-duration="1" class="purecounter"></span>
                    <p>Total beneficiaries</p>
                </div>

                <div class="col-lg-3 col-6 text-center">
                    <span data-purecounter-start="0" data-purecounter-end="374" data-purecounter-duration="1" class="purecounter"></span>
                    <p>Sponsored</p>
                </div>

                <div class="col-lg-3 col-6 text-center">
                    <span data-purecounter-start="0" data-purecounter-end="236" data-purecounter-duration="1" class="purecounter"></span>
                    <p>Youths</p>
                </div>

                <div class="col-lg-3 col-6 text-center">
                    <span data-purecounter-start="0" data-purecounter-end="9" data-purecounter-duration="1" class="purecounter"></span>
                    <p>Trainers</p>
                </div>

            </div>

        </div>
    </section><!-- End Counts Section -->

    <!-- ======= Testimonials Section ======= -->
    <section id="testimonials" class="testimonials">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Testimonials</h2>
                <p>What are they saying</p>
            </div>

            <div class="testimonials-slider swiper-container" data-aos="fade-up" data-aos-delay="100">
                <div class="swiper-wrapper">

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img src="assets/images/mike.png" class="testimonial-img" alt="">
                                <h3>Michael Otieno</h3>
                                <h4>Project Director</h4>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                   Thank you for being interested to know about Kiamauti CDC , we assure our partners and stakeholders of excellent service delivery to array_fill
that need our services in the society.To archive excellent service delivery to humanity we invite all willing partners,collaborators,well wishers and volunteers to join hands with us in any way they can to achieve this goal. Thank you and welcome to Kiamauti CDC                                         <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div><!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img src="assets/img/health.jpg" class="testimonial-img" alt="">
                                <h3>Judith Gitonga</h3>
                                <h4>Project Health Worker</h4>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    In the physical domain we teach our beneficiaries to choose good health practices ,and be physically healthy.
                  We also teach our beneficiaries to interact with other people in a healthy and friendly manner. we protect our beneficiaries
                  from all forms, of abuse because we believe protection of children is central to Gods heart for them .Our approach to child protection begins with preventing abuse as possible.
                  We commit to protecting the children in our centre from all sort of abuse and exploitation ,to intervene quickly and to seek restoration and healing for children affected by abuse.                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div><!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img src="assets/img/makunyi.jpg" class="testimonial-img" alt="">
                                <h3>Zakayas Makunyi</h3>
                                <h4>Project Accountant</h4>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    Hebrew 4:13 Nothing in all creation is hidden from God's sight.Everything is uncovered and laid bare before the eyes of Him to whom
                  we must give account,
                  Commitment to simplicity in accountability, easy to be understood by the led nd leading teams is our passion.We serve knowing we shall finally give an account to God                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div><!-- End testimonial item -->

                  

                  
                </div>
                <div class="swiper-pagination"></div>
            </div>

        </div>
    </section><!-- End Testimonials Section -->

</main><!-- End #main -->

<?php
require_once 'footer.php'
?>