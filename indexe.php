<?php include 'sendEmail.php'; ?>


 <div class="contact-title">
  <h1 class='text-center mt-5'>Send Us An Email</h1>
  <h4 class='send-notification'></h4>
 </div>



 <form id='myForm' method='post' action='' class='container col-md-6'>
  <div class="mb-3">
    <label for="name" class="form-label">Name</label>
    <input name='name' id='name' type="text" class="form-control" aria-describedby="emailHelp" placeholder='Enter your name' required>
  </div>
  
  <div class="mb-3">
    <label for="subject" class="form-label">Subject</label>
    <input name='subject' id='name' type="text" class="form-control" aria-describedby="emailHelp" placeholder='Enter subject' required>
  </div>
  <div class="mb-3">
    <label for="email" class="form-label">Email address</label>
    <input name='email' id='email' type="email" class="form-control"  aria-describedby="emailHelp" placeholder='Enter your email' required>
  </div>

  <div class="mb-3">
    <label for="message" class="form-label">Message</label>
    <textarea name='message' id='body' name="body" cols='90' rows="4" placeholder='Enter message'></textarea>
  </div>
  <div class="mb-3">
  <input type="submit" value='Send message' class="btn btn-success" name='submit'>
  </div>
   <?php echo $alert; ?>
</form>



 <script type='text/javascript'>

if(window.history.replaceState){
 window.history.replaceState(null, null, window.location.href);
}

</script>

 <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
